<?php

namespace Drupal\lbam\Plugin\LayoutSettings;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\layout_settings\Plugin\LayoutSettingsBase;
use Drupal\layout_settings\Plugin\LayoutSettingsManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @LayoutSettings(
 *  id = "anchor_menu",
 *  label = @Translation("Anchor menu"),
 *  context = {
 *    "layout",
 *  }
 * )
 */
class AnchorMenu extends LayoutSettingsBase {

  use StringTranslationTrait;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LayoutSettingsManager $layout_settings_manager, Request $request, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $layout_settings_manager);
    $this->request = $request;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.layout_settings'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(&$build, $context) {
    $region = $context[1];
    if ($region === 'layout') {
      $build['#parent_attributes']['id'] = $build['#settings']['section_uuid'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultValues() {
    return self::defaultSettings() + [
        'label' => [
          'value' => '',
          'format' => filter_default_format(),
        ],
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array &$form, FormStateInterface $form_state) {
    $element = [];
    $data = $this->configuration;

    $element['label'] = [
      '#title' => $this->t('Label'),
      '#type' => 'text_format',
      '#default_value' => !empty($this->configuration['label']['value']) ? $this->configuration['label']['value'] : NULL,
      '#format' => !empty($this->configuration['label']['format']) ? $this->configuration['label']['format'] : filter_default_format(),
    ];

    return $element;
  }

}

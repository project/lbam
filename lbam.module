<?php

/**
 * @file
 * Contains lbam.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function lbam_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the lbam module.
    case 'help.page.lbam':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Layout Builder Anchor Manu') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function lbam_theme($existing, $type, $theme, $path) {
  return [
    'layout_builder_anchor_menu' => [
      'render element' => 'element',
    ],
  ];
}

/**
 * Implements hook_entity_view().
 */
function lbam_entity_view(array &$build, \Drupal\Core\Entity\EntityInterface $entity, \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display, $view_mode) {
  $current_route_name = \Drupal::routeMatch()->getRouteName();
  $route_name = "entity.{$entity->getEntityTypeId()}.canonical";
  if ($current_route_name !== $route_name || $view_mode !== 'full') {
    return;
  }
  if ($entity->hasField('layout_builder__layout') && !$entity->get('layout_builder__layout')
      ->isEmpty()) {
    $sections = $entity->get('layout_builder__layout')->getSections();
  }
  else {
    $sections = $display->getSections();
  }

  $anchor_menu = [];
  /** @var \Drupal\layout_builder\Section $section */
  foreach ($sections as $section) {
    $config = $section->getLayout()->getConfiguration();
    if (empty($config['icon_map_settings']['layout']['layout']['anchor_menu_status'])) {
      continue;
    }
    $uuid = $config['section_uuid'];
    $item = [
      '#type' => 'processed_text',
      '#text' => $config['icon_map_settings']['layout']['layout']['anchor_menu']['label']['value'],
      '#format' => $config['icon_map_settings']['layout']['layout']['anchor_menu']['label']['format'],
      '#filter_types_to_skip' => [],
    ];
    $anchor_menu[$uuid] = \Drupal::service('renderer')->render($item);
  }

  if (!empty($anchor_menu)) {
    $context = [
      'entity' => clone $entity,
      'display' => clone $display,
      'view_mode' => $view_mode,
      'sections' => $sections,
    ];
    \Drupal::moduleHandler()->alter('lbam_anchor_menu', $anchor_menu, $context);
    \Drupal::theme()->alter('lbam_anchor_menu', $anchor_menu, $context);
  }

  if (!empty($anchor_menu)) {
    $build['#theme_wrappers'] = ['layout_builder_anchor_menu'];
    $build['#anchor_menu'] = $anchor_menu;
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function lbam_theme_suggestions_layout_builder_anchor_menu(array $variables) {
  $suggestions = [];
  $entity_type = $variables['element']['#entity_type'];
  $entity = $variables['element']['#' . $entity_type];
  $sanitized_view_mode = strtr($variables['element']['#view_mode'], '.', '_');

  $suggestions[] = 'layout_builder_anchor_menu__' . $entity_type . '__' . $sanitized_view_mode;
  $suggestions[] = 'layout_builder_anchor_menu__' . $entity_type . '__' . $entity->bundle();
  $suggestions[] = 'layout_builder_anchor_menu__' . $entity_type . '__' . $entity->bundle() . '__' . $sanitized_view_mode;
  $suggestions[] = 'layout_builder_anchor_menu__' . $entity_type . '__' . $entity->id();
  $suggestions[] = 'layout_builder_anchor_menu__' . $entity_type . '__' . $entity->id() . '__' . $sanitized_view_mode;

  return $suggestions;
}

/**
 * Implements hook_preprocess_layout_builder_anchor_menu().
 */
function template_preprocess_layout_builder_anchor_menu(&$variables) {
  $variables['children'] = $variables['element']['#children'];

  $list = [];
  foreach ($variables['element']['#anchor_menu'] as $id => $item) {
    $options = ['fragment' => $id];
    $list[] = [
      '#markup' => \Drupal\Core\Link::createFromRoute($item, '<current>', [], $options)
        ->toString(),
    ];
  }

  $variables['anchor_menu'] = [
    '#theme' => 'item_list',
    '#list_type' => 'ol',
    '#items' => $list,
  ];
}
